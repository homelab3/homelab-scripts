#!/bin/bash
# This script will initiliaze the prod tool virtual machine

# Install standard tools
apt install \	
	git \
	ssh

# Terraform
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
apt install terraform

# Ansible

apt install ansible
