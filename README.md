# scripts

Scripts for the homelab automation.

## Reboot

This script reboots the VM.

## Update

This scripts check which distribution is installed and update the whole system.

## Update image

This scripts update the ISO images of the different distribution used for terraform.
