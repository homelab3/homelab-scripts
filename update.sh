#!/bin/bash
# This script check which distribution is running on the system (Manjaro, Ubuntu or Centos) and updates it.

OS=$(grep "^ID=" /etc/os-release)
OS=$(sed -e 's#.*=\(\)#\1#' <<<$OS)
OS=$(sed 's/"//g' <<<$OS)

# Check if the OS corresponds to a known one, if not exit with error
[[ $OS != "manjaro" && $OS != "ubuntu" && $OS != "centos" ]] && ( printf "OS not supported\n" ; exit 1 )

case $OS in
manjaro)
    sudo pacman -Syu 
    ;;
ubuntu)
    sudo apt update && sudo apt upgrade -y
    ;;
centos)
    sudo dnf update -y
    ;;
esac
