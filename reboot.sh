#!/bin/bash
# This script reboots the VM after a random time of $rand seconds (from 0 to 5400 seconds).

MAX_RANGE=5400
rand=$RANDOM
let "rand %= $MAX_RANGE"

sleep $rand

sudo reboot
